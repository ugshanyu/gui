import React, { useState } from "react";
// import { BrowserRouter as Route, Router } from "react-router-dom";
import Registeration from "./Registeration";
// import { browserHistory } from "react-router";
import { Redirect } from "react-router-dom";
import AddUser from "./AddUser";





function LoginForm(props){

	const [details, setDetails] = useState({name:"", email:"", password:""});
	var [error, setError] = useState();

	


	const authenticate = async e => {
		e.preventDefault();
		try {
			const body = {"email":details.email,"regNo":details.name,"userPassword":details.password};
			const response = await fetch("http://localhost:5000/authenticateUser",{
				method: "Post",
				headers: {"Content-Type": "application/json"},
				body: JSON.stringify(body)
			});
			const userData = await response.json();
			if(userData.auth==true && userData.role=="admin"){
				localStorage.setItem("token", userData.token);
				props.history.push('/admin');
			} else if(userData.auth==true && userData.role!="admin"){
				localStorage.setItem("token", userData.token);
				props.history.push('/user');
			} else {
				error = "Оруулсан мэдээлэл буруу"
				setError(error)
				console.log(error)
			}
		} catch (err) {
			console.log(err.message);
		}
	}


	return (
		<form onSubmit={authenticate} >
		<div className = "form-inner">
		<h2>Нэвтрэх</h2>
		
		<div className = "Error">
		{(error != "") ? (<div>{error}</div>) : ("")}
		</div>
		

		<div className = "form-group">
		<label htmlFor="email"> Email: </label>
		<input type="text" name="email" id="email" onChange={e => setDetails({...details, email: e.target.value})} value = {details.email} />
		</div>

		<div className = "form-group">
		<label htmlFor="password"> Password: </label>
		<input type="password" name="password" id="password" onChange={e => setDetails({...details, password: e.target.value})} value = {details.password} />
		</div>

		<input type="submit" value="нэвтрэх" />

		</div>
		</form>

		)
}

export default LoginForm;
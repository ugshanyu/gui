import React, { useState, useEffect } from 'react'

function User(props) {
	const [details, setDetails] = useState({regNo:"", email:"", password:""});

	useEffect(() => {
		checkToken(props)
    myInfo()
    }, [props])

	async function  myInfo(){
		const body = {"token":localStorage.getItem("token")};
		const response = await fetch("http://localhost:5000/myInfo",{
			method: "Post",
			headers: {"Content-Type": "application/json"},
			body: JSON.stringify(body)
		})
		const serverResponse = await response.json();
		if(serverResponse.auth=="succeeded"){
			setDetails({regNo:serverResponse.regNo, email:serverResponse.email, password:""})
		}
	}

	function checkToken(props) {
    	if(localStorage.getItem("token")==""){
    		alert("Дахин нэвтрнэ үү");
    		props.history.push('/');
    	}
    }

	async function  updateMyInfo(){
		const body = {email:details.email,regNo:details.regNo,userPassword:details.password,"token":localStorage.getItem("token")};
		const response = await fetch("http://localhost:5000/updateMyInfo",{
			method: "Post",
			headers: {"Content-Type": "application/json"},
			body: JSON.stringify(body)
		})
		const serverResponse = await response.json();
		if(serverResponse.response=="succeeded"){
			alert("Амжилттай шинэчлэгдлээ");
			myInfo();
		} else{
			alert("Алдаа");
		}
	}

	const handleSubmit = e =>{
		e.preventDefault()
		if(details.email!=""&&details.regNo!=""){
			updateMyInfo();
		} else{
			alert("Мэдээлэл дутуу")
		}
		
	}

	const exit = async e => {
		e.preventDefault();
		localStorage.setItem("token", "");
		props.history.push('/');
	}

	return (
		<div className="Registeration">
		<form onSubmit={handleSubmit}>
		<div className = "form-inner">
		<h2>Мэдээлэл шинэчлэх</h2>
		<div className = "Error">
		</div>
		

		<div className = "form-group">
		<label htmlFor="email"> Email: </label>
		<input type="text" name="email" id="email" onChange={e => setDetails({...details, email: e.target.value})} value = {details.email} />
		</div>

		<div className = "form-group">
		<label htmlFor="regNo"> Регистр: </label>
		<input type="regNo" name="regNo" id="regNo" onChange={e => setDetails({...details, regNo: e.target.value})} value = {details.regNo} />
		</div>

		<div className = "form-group">
		<label htmlFor="password"> Password: </label>
		<input type="password" name="password" id="password" onChange={e => setDetails({...details, password: e.target.value})} value = {details.password} />
		</div>

		<input type="submit" value="шинэчлэх" />

		</div>
		</form>
		<div className="Add">
				 <button className="ButtonExit" onClick={exit}>Гарах</button>
			</div>
		</div>
		)
}

export default User
import React, {useState,useMemo, useEffect} from "react";
import {AgGridColumn, AgGridReact} from 'ag-grid-react';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-alpine.css';
import { IconName } from "react-icons/bi";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTrash, faPlus, faTimes } from '@fortawesome/free-solid-svg-icons'

function MyRenderer(params) {
    return (
    	<div><FontAwesomeIcon icon={faTrash} /></div>
          
    );
}

function onCellValueChanged(params) {
  var changedData = [params.data];
  params.api.applyTransaction({ update: changedData });
  console.log(changedData);
  var userPassword = changedData[0].userPassword
  if(userPassword==null){
  	userPassword = null;
  }
  updateUserInfo({email:changedData[0].email,regNo:changedData[0].reg_no, userPassword:userPassword, userId:changedData[0].user_id, token:localStorage.getItem("token")})
}

async function  updateUserInfo(body){
	const response = await fetch("http://localhost:5000/updateUserInfo",{
		method: "Post",
		headers: {"Content-Type": "application/json"},
		body: JSON.stringify(body)
	})
	const serverResponse = await response.json();
	if(serverResponse.response=="succeeded"){
		alert("Амжилттай шинэчлэгдлээ");
	} else{
		alert("Алдаа");
	}
}

    

function Registeration(props){

	const [rowData, setRowData] = useState([]);

	const columnDefs = useMemo( ()=> [
        { colId: "email", field: 'email' , filter: 'agTextColumnFilter', onCellValueChanged: onCellValueChanged,editable: true, sortable: true,floatingFilter: true,width:300},
        { colId: "regNo", headerName:"RegNo",field: 'reg_no', filter: 'agTextColumnFilter', onCellValueChanged: onCellValueChanged, editable: true, sortable: true, floatingFilter: true,width:250},
        { colId: "userPassword", headerName:"Password",field: 'userPassword', filter: 'agTextColumnFilter', onCellValueChanged: onCellValueChanged, editable: true, sortable: true, floatingFilter: true,width:245},
        {
          width: 50,
          cellRendererFramework: MyRenderer,
          editable: false,
          colId: "action",
          textAlign: 'center'
        }

    ], []);

    const gridOptions = {
    	onCellClicked: (event: CellClickedEvent) => event.colDef.colId=="action"?deleteUser(event.data.user_id):console.log("")
    }

    async function  deleteUser(params){
	const body = {"id":params,"token":localStorage.getItem("token")};
	const response = await fetch("http://localhost:5000/delete",{
		method: "Post",
		headers: {"Content-Type": "application/json"},
		body: JSON.stringify(body)
	})
	const serverResponse = await response.json();
	if(serverResponse.deleted==true){
		getUsers();
	}
}

    function getUsers() {
    	const body = {"token":localStorage.getItem("token")};
		fetch("http://localhost:5000/Users",{
				method: "Post",
				headers: {"Content-Type": "application/json"},
				body: JSON.stringify(body)
			})
		.then(result => result.json())
		.then(rowData => rowData.data!=null ? setRowData(rowData.data):console.log("aldaa") )
    }

    async function checkToken(props) {
    	if(localStorage.getItem("token")==""){
    		alert("Дахин нэвтрнэ үү");
    		props.history.push('/');
    	} else {
    		const body = {"token":localStorage.getItem("token")};
			const response = await fetch("http://localhost:5000/checkToken",{
				method: "Post",
				headers: {"Content-Type": "application/json"},
				body: JSON.stringify(body)
			})
			const serverResponse = await response.json();
			if(serverResponse.response!="succeeded"){
				props.history.push('/');
			}
    	}
    }

    async function  deleteUser(params){
	const body = {"id":params,"token":localStorage.getItem("token")};
	const response = await fetch("http://localhost:5000/delete",{
		method: "Post",
		headers: {"Content-Type": "application/json"},
		body: JSON.stringify(body)
	})
	const serverResponse = await response.json();
	if(serverResponse.deleted==true){
		getUsers();
	}
}


	useEffect(() => {
		checkToken(props)
		getUsers();
		
	}, [props]);

	const addUser = async e => {
		e.preventDefault();
		props.history.push('/AddUser');
	}

	const exit = async e => {
		e.preventDefault();
		localStorage.setItem("token", "");
		props.history.push('/');
	}

	return  (
		<div className="Registeration">
			<div className="List">
				<div className="ag-theme-alpine" style={{height: 500, width: 850}}>
					<AgGridReact 
					gridOptions = {gridOptions}
					rowData={rowData}
					columnDefs={columnDefs}>
					</AgGridReact>
				</div>
			</div>
			<div className="Add">
				 <button className="ButtonExit" onClick={exit}>Гарах</button>
				 <button className="Button" onClick={addUser}><FontAwesomeIcon className="Icon" icon={faPlus} />Шинэ хэрэглэгч</button>
			</div>
		</div>
		)
}



export default Registeration;
import React , {useState,useEffect} from 'react'
import {BrowserRouter as Router,Route,Redirect,Switch} from 'react-router-dom';

export default function AddUser(props) {
	const [details, setDetails] = useState({regNo:"", email:"", password:""});

	const goBack = async e => {
		e.preventDefault();
		props.history.push('/admin');
	}

	useEffect(() => {
		checkToken(props)
	}, [props]);

	function checkToken(props) {
    	if(localStorage.getItem("token")==""){
    		alert("Дахин нэвтрнэ үү");
    		props.history.push('/');
    	}
    }

	async function  register(){
		try {
			const body = {"email":details.email,"regNo":details.regNo,"userPassword":details.password,"token":localStorage.getItem("token")};
			const response = await fetch("http://localhost:5000/newUser",{
				method: "Post",
				headers: {"Content-Type": "application/json"},
				body: JSON.stringify(body)
			});
			const userData = await response.json();
			if(userData.response=="succeeded"){
				alert("Амжилттай хадгаладлаа!");
				setDetails({regNo:"", email:"", password:""});
			} else{
				alert("Алдаа");
			}
			// if(userData.auth==true && userData.role=="admin"){
			// 	localStorage.setItem("token", userData.token);
			// 	props.history.push('/admin');
			// } else {
			// 	error = "Оруулсан мэдээлэл буруу"
			// 	setError(error)
			// 	console.log(error)
			// }
		} catch (err) {
			console.log(err.message);
		}
	}

	const handleRegister = e => {
		e.preventDefault();
		if(details.email!=""&&details.regNo!=""&&details.password!=""){
			register();
		} else{
			alert("Мэдээлэл дутуу")
		}
		
	}
	return (
		<div className="Registeration">
		<form  onSubmit={handleRegister}>
			<div className = "form-inner">
				<h2>Бүртгэл</h2>
		
{/*				<div className = "Error">
					{(error != "") ? (<div>{error}</div>) : ("")}
				</div>*/}
		

				<div className = "form-group">
					<label htmlFor="email"> Email: </label>
					<input type="text" name="email" id="email" onChange={e => setDetails({...details, email: e.target.value})} value = {details.email} />
				</div>

				<div className = "form-group">
					<label htmlFor="regNo"> Регистр: </label>
					<input type="regNo" name="regNo" id="regNo" onChange={e => setDetails({...details, regNo: e.target.value})} value = {details.regNo} />
				</div>

				<div className = "form-group">
					<label htmlFor="password"> Password: </label>
					<input type="password" name="password" id="password" onChange={e => setDetails({...details, password: e.target.value})} value = {details.password} />
				</div>

				<input type="submit" value="бүртгэх" />

			</div>
		</form>
		<div className="Add">
				 <button className="Button" onClick={goBack}>Буцах</button>
			</div>
		</div>
	)
}
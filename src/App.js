import React, {useState, Fragment} from 'react';
import {BrowserRouter as Router,Route,Redirect,Switch} from 'react-router-dom';
import LoginForm from './components/LoginForm';
import Registeration from "./components/Registeration";
import AddUser from "./components/AddUser";
import User from "./components/User";


function App() {

  const [user, setUser] = useState({name:"", email:""});
  const [error, setError] = useState("");
  const [userRegno, setUserRegNo] = useState({regNo:""});



  return (


    <Router>
      <div className = "App">
        <Switch>
          <Route path="/User" component = { User } />
          <Route path="/addUser" component = { AddUser } />
          <Route path="/admin" component={ Registeration } />
          <Route path="/" component = { LoginForm } />
        </Switch>
      </div>
    </Router>


/*    <div className="App">
      {(user.email != "") ? (
        <div className = "Welcome">
          <h2>Welcome <span>{user.name}</span></h2>
          <button onClick={Logout} >Logout</button>
        </div>
        ) : (
        <LoginForm Login={Login} error={error} />)
      }
    </div>*/
  );
}

export default App;
